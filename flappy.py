
"""Flappy, game inspired by Flappy Bird.

Exercises

1. Keep score.
2. Vary the speed.
3. Vary the size of the balls.
4. Allow the bird to move forward and back.

"""

import random as rand
from random import *
from turtle import *
from freegames import vector

bird = vector(0, 0)
balls = []
i = 0
ball_size = rand.randint(20, 100)
random_speed = rand.randint(30, 100)


def tap(x, y):
    "Move bird up in response to screen tap."
    up = vector(0, 30)
    bird.move(up)
    global i
    i += 1
    print("Your score -", i)


def change(x, y):
    bird.x += x
    global i
    i += 1
    print("Your score -", i)

def inside(point):
    "Return True if point on screen."
    return -200 < point.x < 200 and -200 < point.y < 200


def draw(alive):
    "Draw screen objects."
    clear()

    goto(bird.x, bird.y)

    if alive:
        dot(10, 'green')
    else:
        dot(10, 'red')

    for ball in balls:

        goto(ball.x, ball.y)
        dot(ball_size, 'black')

    update()


def move():
    "Update object positions."
    bird.y -= 5

    for ball in balls:
        ball.x -= 3

    if randrange(10) == 0:
        y = randrange(-199, 199)
        ball = vector(199, y)
        balls.append(ball)

    while len(balls) > 0 and not inside(balls[0]):
        balls.pop(0)

    if not inside(bird):
        draw(False)
        return

    for ball in balls:
        if abs(ball - bird) < 15:
            draw(False)
            return

    draw(True)
    ontimer(move, random_speed)


setup(420, 420, 370, 0)
hideturtle()
up()
tracer(False)
onscreenclick(tap)
listen()
onkey(lambda: change(30, 0), 'Right')
onkey(lambda: change(-30, 0), 'Left')
move()
done()
